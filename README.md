# Dockerfile
Source for building [https://hub.docker.com/repository/docker/krouma/theduels](https://hub.docker.com/repository/docker/krouma/theduels). It is a container with httpd, PHP and this application.

# docker-compose.yml
The easiest way to run TheDuels in Docker container is to download this file and run `docker-compose up -d`. Then the app is available on port 80 and Adminer on port 8080 on the host machine. The app's default password for admin section in `/admin` is `admin` and changing it should be the first thing you do.

